# Netbox Utility Documentation

To update the documentation structure run the following command from docs directory:
sphinx-apidoc -f -o source/ ../netboxutils

To make the documentation run the following command from docs directory:
make html
