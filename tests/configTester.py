from docs import baseClass
from docs import Configuration, set_conf_path
from docs import validate_config_path


def main():

        path = set_conf_path()

        if validate_config_path(path):
            baseClass._path = path
            configTest = Configuration()

            option = configTest.read_config("Netbox", "api_token")
            print(configTest, option)



if __name__ == '__main__':
    main()