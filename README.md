## Netbox Utility

**Netbox Utility** is an **open source** utility that extends a data center infrastructure management tools called _Netbox_
which allows to update infrastructure device from a json file obtained when running the '-lshw' command on your servers.


### Pre-requisites
Follow instructions on how to install Netbox @https://netbox.readthedocs.io/en/latest/
Follow instructions on how to install pdflatex for linux @https://gist.github.com/rain1024/98dd5e2c6c8c28f9ea9d


### Download
Download Netbox-utils  source code @https://bitbucket.org/mendela38/netbox-utility/src/master/


### Installation

From the checkout of this repo and withing your virtual environment (if you use one) use pip to install.

**Step 1**
Navigate to root folder of the project from terminal which should be
```
cd <path to root>/netbox-utility
```
**Step 2**
Execute this command (_Refer to Important Note below_)
```
source venv/bin/activate pip install -U
```

**Step 3**
Navigate to netboxtutility folder
```
cd netboxtutility
```

**Step 4**
Now execute program by parsing commands (_Refer to Important Note below_)
```
python netbox_utils.py <command options>
eg: python netbox_utils.py -h
```

**Important Note:**
On systems with Python2 and Python3 installed please use _**pip3**_ instead of pip and _**python3**_ instead on python.


### NetBox-Utils' commands

```
usage: Netbox-Utils [-h] [-v] [-c] device_name path [config]

List of commands of NetBox-Utils.

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit

General Command:
  device_name    Pass the the device name to update on Netbox.
  path           Pass file path to sync with netbox.
  -c, --config   Set configuration path.
  config         Specify configuration file to use. If not set defaults to current directory
```