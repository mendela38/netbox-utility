#!/usr/bin/env python3

from setuptools import find_packages, setup

tests_require = [
    'pytest', 'pytest-mock', 'pytest-capturelog',
    'pytest-cov', 'coverage'
]

docs_require = [
    'sphinx', 'sphinx-autodoc-typehints',
    'sphinxcontrib-napoleon'
]

setup(
    name='netboxutils',
    description="Nelson please edit",
    author="Nelson Dick Kelechi",
    author_email="ndick4@gmail.com",
    packages=find_packages(),
    url="https://bitbucket.org/mendela38/netbox-utility",
    download_url="https://bitbucket.org/mendela38/netbox-utility",
    license='BSD',
    scripts=[
        # 'netboxutils/notifications/notify.py',
        # 'netboxutils/reports/report.py',
        # 'netboxutils/reports/laTex.py',
        # 'netboxutils/utility/baseClass.py',
        # 'netboxutils/utility/config.py',
        # 'netboxutils/utility/json_utility.py',
        # 'netboxutils/utility/validation.py',
        # 'netboxutils/netbox_utils2.py',
        # 'netboxutils/connection.py',
        'netboxutils/netbox_utils.py'
    ],
    setup_requires=['katversion', 'pytest-runner', 'requests'],
    use_katversion=True,
    install_requires=[
        'pynetbox'
    ],
    tests_require=tests_require,
    extras_require={
        'test': tests_require,
        'doc': docs_require,
    },
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
    ],
    platforms=["OS Independent"],
    keywords="netbox dcim",
    python_requires='>=3.5',
    zip_safe=False)
