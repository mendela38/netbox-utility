#!/usr/bin/env python3

import argparse
import sys

# https://chase-seibert.github.io/blog/2014/03/21/python-multilevel-argparse.html
# Author: Chase Seibert (Engineering Manager at Dropbox)

from baseClass import BaseClass, timestamp
from connection import ApiUtility
from reports.rackReport import RackReport
from utilities.config import set_conf_path
from utilities.validation import validate_json_path, validate_config_path, validate_device_name, validate_item


class NetBoxUtilCommand(object):

    def __init__(self):
        # TODO
        parser = argparse.ArgumentParser(prog="Netbox-utils", description='Short description of netbox utils here',
                                         usage='''netbox-utils [-h] [-v] <command> [<args>]
The most commonly used git commands are:
List of inventory commands
    find    Query Netbox to check on device.
    add     Record changes to the repository.
    update      Download objects and refs from another repository.
List of reporting Commands
    rack    Generate Graphical representation of a specify rack.
    server    Generate Graphical representation of a specify rack.

''')

        # Version of the program
        parser.add_argument("-v", '--version', help="Show the current version of Netbox-Utils", action='version',
                            version="Nelson Dick K.\n %(prog)s 2.0")

        # Sub-Command to execute
        parser.add_argument('command', help='Subcommand to run')
        # parse_args defaults to [1:] for args, but you need to
        # exclude the rest of the args too, or validation will fail
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            parser.print_help()
            print('Unrecognized command')
            exit(1)
        try:
            # use dispatch pattern to invoke method with same name
            getattr(self, args.command)()
        except Exception as err:
            print(err.args[0])

    @staticmethod
    def add():
        parser = argparse.ArgumentParser(description='Add Inventory for a specify device.')
        parser.add_argument('device_name', nargs=1, help="Device name that need to be updated.")
        parser.add_argument('path', nargs=1, type=argparse.FileType('r'), help="JSON File path to sync with DCIM.")
        parser.add_argument('config', nargs='?', help="Path to Configuration file", default="current")
        # now that we're inside a subcommand, ignore the first
        # TWO argvs, ie the command (git) and the subcommand (commit)
        args = parser.parse_args(sys.argv[2:])

        # Set Configuration Path, Device name & Json Path
        BaseClass.config_path = set_conf_path(args.config)
        BaseClass.json_path = args.path[0].name
        BaseClass.device_name = args.device_name[0]

        if NetBoxUtilCommand.validate():
            # Initialize Api instance
            api = ApiUtility()

            api.create_inventories()

    @staticmethod
    def validate():
        # Validate Json path
        if validate_json_path():

            # Validate Configuration Path
            if validate_config_path():

                # Validate device name
                if validate_device_name():
                    return True

    @staticmethod
    def find():
        parser = argparse.ArgumentParser(description='Check if device exist in your netbox')
        parser.add_argument('device_name', help="Device name that need to be updated.")
        parser.add_argument('config', nargs='?', help="Path to Configuration file", default="current")
        args = parser.parse_args(sys.argv[2:])
        BaseClass.device_name = args.device_name
        BaseClass.config_path = set_conf_path(args.config)
        if validate_device_name():
            print("Device named '%s' exist." % args.device_name)

    @staticmethod
    def update():
        parser = argparse.ArgumentParser(description='Update inventory of a specific device.')
        parser.add_argument('device_name', nargs=1, help="Device name that need to be updated.")
        parser.add_argument('path', nargs=1, type=argparse.FileType('r'), help="JSON File path to sync with DCIM.")
        parser.add_argument('config', nargs='?', help="Path to Configuration file", default="current")
        args = parser.parse_args(sys.argv[2:])

        # Set Configuration Path, Device name & Json Path
        BaseClass.config_path = set_conf_path(args.config)
        BaseClass.json_path = args.path[0].name
        BaseClass.device_name = args.device_name[0]

        # Validate Json path
        if NetBoxUtilCommand.validate():
            # Initialize Api instance
            api = ApiUtility()
            api.update_inventories()

    @staticmethod
    def rack():
        parser = argparse.ArgumentParser(description='Update inventory of a specific device.')
        parser.add_argument('rack_name', help="Device name that need to be updated.")
        parser.add_argument('config', nargs='?', help="Path to Configuration file", default="current")
        parser.add_argument("-pdf", "--pdf", help="Export of pdf file", action="store_true")
        parser.add_argument("-latex", "--latex", help="Export to latex file", action="store_true")
        parser.add_argument("-email", "--email", help="Send report generated via email", action="store_true")

        args = parser.parse_args(sys.argv[2:])

        # Set configuration path
        BaseClass.config_path = set_conf_path(args.config)

        # Validate that rack_info exists
        if validate_item("Rack", args.rack_name):
            report_name = "Rack Report - %s_%s" % (args.rack_name, timestamp())
            report = RackReport(report_name)
            report.generate_rack_report()
            if args.pdf:
                report.pdf_export(mail=args.email, name=report_name)

            if args.latex:
                report.latex_export(mail=args.email, name=report_name)


if __name__ == '__main__':
    NetBoxUtilCommand()
