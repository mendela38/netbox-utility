#!/usr/bin/env python3
from pylatex import Document, Head, Foot, simple_page_number

from netboxutils.baseClass import BaseClass, timestamp
from netboxutils.reports.rackReport import RackReport
from netboxutils.utilities.validation import validate_device_name, validate_rack_name, validate_item


def main():
    #  Set configuration path
    BaseClass.config_path = "/home/nelson/Documents/netbox-utility/config.ini"

    BaseClass.json_path = "/home/nelson/Documents/netbox-utility/data/hw.json"

    BaseClass.device_name = "C8-PDU1"

    # Validate that rack exists
    rack_name = "R1"
    if validate_item("Rack", rack_name):
        report_name = "Rack Report - %s_%s" % (rack_name, timestamp())
        report = RackReport(report_name)
        report.generate_rack_report()
        report.pdf_export(mail=True, name=report_name)


if __name__ == '__main__':
    main()
