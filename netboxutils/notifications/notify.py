#!/usr/bin/env python3

import time

import requests
import datetime


def formatted_message(message, _state):
    """

    :rtype: str
    """
    return "%s - %s - %s" % (timestamp(), state(_state), message)


def timestamp():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')


def state(_state):
    if _state:
        return "[PASS]"
    else:
        return "[FAIL]"


class Notify:

    def __init__(self, _url=None, _message=''):
        self.__url = _url
        self.__message = _message

    def get_url(self):
        return self.__url

    def set_url(self, url):
        self.__url = url

    def get_message(self):
        return {
            "message": self.__message
        }

    def build_message(self, message, _state=True):
        """
        Build and format message to be send via webhook
        :param message: Message to be append
        :param _state: S
        :return:
        """
        self.__message += "%s\n" % formatted_message(message, _state)
        return self

    @property
    def send(self):
        """
        Send message via webhook
        :return:
        """
        try:
            return requests.post(self.get_url(), self.get_message())
        except requests.RequestException as err:
            raise requests.RequestException(err.args[0])
