#!/usr/bin/env python3

import json

import pynetbox

from slugify import slugify

from baseClass import BaseClass


def validate_existence(data, key="name", expected_value=""):
    for _type in data:
        if _type[key].lower() in expected_value.lower():
            return True
    return False


def inventory_field(current_disk, device_id, manufacturer_id):
    return {
        "device": device_id,
        "name": current_disk['id'],
        "manufacturer": manufacturer_id,
        "part_id": current_disk['version'],
        "serial": current_disk['serial'],
        "asset_tag": "%s-%s" % (current_disk['vendor'], current_disk['serial']),
        "discovered": True,
        "description": "%s - %s" % (current_disk['description'], get_(current_disk['capacity'], current_disk['units']))
    }


def inventory_cpu_field(current_cpu, device_id, manufacturer_id):
    return {
        "device": device_id,
        "name": current_cpu['id'],
        "manufacturer": manufacturer_id,
        'part_id': current_cpu['version'],
        "serial": current_cpu['handle'],
        "asset_tag": "%s-%s" % (current_cpu['vendor'], current_cpu['handle']),
        "discovered": True,
        "description": "%s - %s" % (current_cpu['description'], get_(current_cpu['capacity'], current_cpu['units'], 5))
    }


def inventory_ram_field(current_ram, device_id, manufacturer_id, capacity):
    return {
        "device": device_id,
        "name": 'ram:%s' % current_ram['physid'],
        "manufacturer": manufacturer_id,
        "discovered": True,
        'part_id': "Slot: %s" % current_ram['slot'],
        "serial": current_ram['serial'],
        "asset_tag": "%s-%s" % (current_ram['vendor'], current_ram['handle']),
        "description": "Capacity of %s but %s is been used" % (
            get_(capacity, current_ram['units'], 3),
            get_(current_ram['size'], current_ram['units'], 5))
    }


########################################################################################################
# https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb/37423778
# Author: Bryan_Rch Updated by: Pietro Battiston and Nelson Dick Kelechi
########################################################################################################
def get_(size, unit, round_value=2):
    # 2**10 = 1024
    power = 2 ** 10
    n = 0
    dic_power_n = {0: '', 1: 'Kilo', 2: 'Mega', 3: 'Giga', 4: 'Tera'}
    while size > power:
        size /= power
        n += 1
    return "%s %s" % (round(size, round_value), dic_power_n[n] + unit)


class ApiUtility(BaseClass):

    # Initialize connection to host via API
    def __init__(self):
        """BaseClass.device_name
        Initialize Connection to Netbox
        """
        super().__init__()

        host_name = self.configuration.read_config("Netbox", "url")
        api_token = self.configuration.read_config("Netbox", "token")

        try:
            self.netbox = pynetbox.api(url=host_name, token=api_token)
        except pynetbox.RequestError as e:
            BaseClass.msgErr = "Could not connect to host: %s with token: %s" % (host_name, api_token)
            print(BaseClass.msgErr)
            raise

    def create_inventory(self, fields):
        """Create device type

        :return: bool True if successful otherwise raise CreateException
        """
        try:
            # Creating inventory
            return self.netbox.dcim.inventory_items.create(fields)
        except pynetbox.RequestError as e:
            error = json.loads(e.error)
            if error['asset_tag'] is not None:
                print("'%s' has already been added to '%s' as an inventory item." % (
                    fields['name'], BaseClass.device_name))
                pass
            else:
                raise pynetbox.RequestError(e.error)

    def get_manufacturer_id(self, manufacturer_name):

        name = self.netbox.dcim.manufacturers.get(name=manufacturer_name)
        if name is None:
            create_manufacturer = input(
                "Manufacturer '%s' does not exist, Would you like to create it (y/n): " % manufacturer_name)
            if create_manufacturer == "y" or create_manufacturer == "yes" or create_manufacturer == '':
                create_name = self.netbox.dcim.manufacturers.create(name=manufacturer_name,
                                                                    slug=slugify(manufacturer_name))
                name = self.netbox.dcim.manufacturers.get(name=create_name['name'])
            else:
                raise Exception("Please add '%s' as Manufacturer." % manufacturer_name)

        return name.serialize()['id']

    def update_inventories(self):
        # Get device id to need to be updated
        device_id = self.netbox.dcim.devices.get(name=self.device_name).serialize()['id']

        # get list of Hard Drives
        disks = self.json.find_by_key_filter(value="disk", filter_key="id", filter_value="disk")

        # Loop through the filtered list of disks extracted from the Json file
        for disk in disks:
            if 'vendor' in disk and 'serial' in disk:
                # Get Hard drive manufacturer
                manufacturer_id = self.get_manufacturer_id(disk['vendor'])

                if self.update_inventory(disk, device_id, manufacturer_id, "disk"):
                    print("Updated Inventory Item '%s' of '%s'" % (disk['id'], self.device_name))
                else:
                    print("No changes applied to HDD inventory '%s' of device '%s'" % (
                        disk['id'], BaseClass.device_name))

        # CPUs
        cpus = self.json.find_by_key_filter(value="processor", filter_key="id", filter_value="cpu")

        # Loop through the filtered list of cpus extracted from the Json file
        for cpu in cpus:
            if 'vendor' in cpu and 'handle' in cpu:
                # Get CPU manufacturer
                manufacturer_id = self.get_manufacturer_id(cpu['vendor'])

                if self.update_inventory(cpu, device_id, manufacturer_id, "cpu"):
                    print("Updated Inventory Item '%s' of '%s'" % (cpu['id'], self.device_name))
                else:
                    print("No changes applied to CPU inventory '%s' of device '%s'" % (
                        cpu['id'], BaseClass.device_name))

        rams = self.json.find_by_key(data=self.json.read_file, key_name="description", key_value="System Memory")
        data = list(rams)

        for ram in data:
            for sub_ram in ram['children']:
                if 'vendor' in sub_ram:
                    # Get RAM manufacturer
                    manufacturer_id = self.get_manufacturer_id(sub_ram['vendor'])

                    if self.update_inventory(sub_ram, device_id, manufacturer_id, "ram", ram['capacity']):
                        print("Updated Inventory Item 'ram:%s' of '%s'" % (sub_ram['physid'], self.device_name))
                    else:
                        print("No changes applied to RAM inventory 'ram:%s' of device '%s'" % (
                            sub_ram['physid'], BaseClass.device_name))

    def create_inventories(self):

        # Get device id to need to be updated
        device_id = self.netbox.dcim.devices.get(name=self.device_name).serialize()['id']

        # get list of Hard Drives
        disks = self.json.find_by_key_filter(value="disk", filter_key="id", filter_value="disk")

        # Loop through the filtered list of disks extracted from the Json file
        for disk in disks:
            if 'vendor' in disk:
                # Get Hard drive manufacturer
                manufacturer_id = self.get_manufacturer_id(disk['vendor'])

                # Data
                _fields = inventory_field(disk, device_id, manufacturer_id)

                # Save disk Inventory item extracted from json file
                if self.create_inventory(fields=_fields):
                    print("Successfully added '%s' to inventory list of '%s'." % (_fields['name'], self.device_name))
                    # self.notify.build_message(
                    #     "Successfully added '%s' to inventory list of '%s'." % (
                    #         _fields['name'], self.device_name)).send()

        # Get list of CPUs
        cpus = self.json.find_by_key_filter(value="processor", filter_key="id", filter_value="cpu")

        # Loop through the filtered list of cpus extracted from the Json file
        for cpu in cpus:
            if 'vendor' in cpu:
                # Get CPU manufacturer
                manufacturer_id = self.get_manufacturer_id(cpu['vendor'])

                _fields = inventory_cpu_field(cpu, device_id, manufacturer_id)

                if self.create_inventory(fields=_fields):
                    print("Successfully added '%s' to inventory list of '%s'." % (_fields['name'], self.device_name))

        rams = list(self.json.find_by_key(data=self.json.read_file, key_name="description", key_value="System Memory"))
        for ram in rams:
            for sub_ram in ram['children']:
                if 'vendor' in sub_ram:
                    # Get RAM manufacturer
                    manufacturer_id = self.get_manufacturer_id(sub_ram['vendor'])

                    _fields = inventory_ram_field(sub_ram, device_id, manufacturer_id, ram['capacity'])

                    if self.create_inventory(fields=_fields):
                        print(
                            "Successfully added '%s' to inventory list of '%s'." % (_fields['name'], self.device_name))
                        # self.notify.build_message("Successfully added '%s' to inventory list of '%s'." % (
                        #     _fields['name'], self.device_name)).send()

    def update_inventory(self, data, device_id, manufacturer_id, inventory_type, capacity=None):

        try:

            if inventory_type == "cpu":
                inventory = self.netbox.dcim.inventory_items.get(serial=data['handle'])
                serial = data['handle']
            else:
                inventory = self.netbox.dcim.inventory_items.get(serial=data['serial'])
                serial = data['serial']

            if inventory is not None:
                if serial == inventory.serialize()['serial']:
                    inventory.device = int(device_id)
                    inventory.manufacturer = int(manufacturer_id)
                    inventory.discovered = True

                    if inventory_type == "cpu":
                        inventory.name = data['id']
                        inventory.part_id = data['version']
                        inventory.serial = data['handle']
                        inventory.asset_tag = "%s-%s" % (data['vendor'], data['handle'])
                        inventory.description = "%s - %s" % (data['description'], get_(data['capacity'], data['units']))
                    elif inventory_type == "ram":
                        inventory.name = 'ram:%s' % data['physid']
                        inventory.part_id = "Slot: %s" % data['slot']
                        inventory.serial = data['serial']
                        inventory.asset_tag = "%s-%s" % (data['vendor'], data['handle'])
                        if capacity is not None:
                            inventory.description = "Capacity of %s but %s is been used" % (
                                get_(capacity, data['units']),
                                get_(data['size'], data['units']))
                    elif inventory_type == "disk":
                        inventory.name = data['id']
                        inventory.part_id = str(data['version'])
                        inventory.serial = str(data['serial'])
                        inventory.asset_tag = str("%s-%s" % (data['vendor'], data['serial']))
                        inventory.description = str(
                            "%s - %s" % (data['description'], get_(data['capacity'], data['units'])))

                        return inventory.save()
                else:
                    raise Exception("Inventory Item with Serial '%s' has been created." % serial)

            #Remove
        except pynetbox.RequestError as e:
            print(e.error)
        # else:
        #     raise pynetbox.RequestError(
        #         "'%s' does not exist as inventory item for '%s'" % (inventory.name, BaseClass.device_name))

