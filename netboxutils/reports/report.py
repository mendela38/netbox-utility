#!/usr/bin/env python3
import subprocess

from pylatex import Document, Section, Subsection, Command, Figure, PageStyle, Head, LineBreak, LargeText, MiniPage, \
    MediumText, VerticalSpace, NoEscape
import os

#######
# General comment regarding Latex
# Follow instructor on the following link: https://latextools.readthedocs.io/en/latest/install/#linux
####
from pylatex.base_classes import command
from pylatex.utils import italic, bold

from baseClass import BaseClass
from connection import ApiUtility
from reports.mailing import Mail
from utilities.validation import is_directory


class Report(ApiUtility):

    def __init__(self, doc_name, orientation='portrait', font='12pt'):
        super().__init__()
        try:
            self.report_path = self.configuration.read_config("Path", "report_path")
            self.doc_name = doc_name
            self.full_path = "%s/%s" % (self.report_path, self.doc_name)

            if not is_directory(self.report_path):
                os.mkdir(self.report_path)

            self.geometry_options = {
                "head": "22pt",
                "margin": "0.5in",
                "bottom": "0.6in",
                "includeheadfoot": True
            }

            self.document = Document(default_filepath=self.full_path, geometry_options=self.geometry_options)
            self.document.documentclass = Command(
                'documentclass',
                options=[font, orientation],
                arguments=['article'],
            )

            self.header = PageStyle("header")

            with self.header.create(Head("C")):
                self.header.append(LargeText(bold(doc_name)))

            self.document.preamble.append(self.header)
            self.document.preamble.append(NoEscape(r'\today'))
            self.document.change_document_style("header")

        except FileExistsError:
            pass
        # except IsADirectoryError:
        #     raise ValueError("Report Path is not set.")

    def set_header(self, header_name, option):
        with self.header.create(option):
            self.header.append(header_name)

    def apply(self):
        self.document.preamble.append(self.header)
        self.document.change_document_style("header")
        return self

    def add_content(self, data):
        if type(data) is str:
            if data:
                self.document.append(data)
            else:
                self.document.append("N/A\n")
        else:
            for info in data:
                self.document.append(bold(info))
                self.document.append(": ")

                if not data[info]:
                    self.document.append(italic("N/A"))
                else:
                    self.document.append(italic(data[info]))
                self.document.append("\n")
        # self.document.append(LineBreak())

        return self

    def create_mini_page(self):
        with self.document.create(MiniPage(width=r"0.5\textwidth")):
            return self

    def add_vertical_space(self, value="20pt"):
        self.document.append(VerticalSpace("20pt"))
        self.document.append(LineBreak())
        return self

    def create_section(self, title):
        with self.document.create(Section(title)):
            return self

    def sub_section(self, title):
        with self.document.create(Subsection(title)):
            return self

    def create_figure(self, image_path, caption):
        try:
            with self.document.create(Figure(position='h!')) as image:
                image.add_image(image_path)
                image.add_caption(caption)
                return self
        except FileNotFoundError:
            BaseClass.msgErr = "Could not find image @path: '%s'" % image_path
            print(BaseClass.msgErr)
            raise
        except subprocess.CalledProcessError as processError:
            print(processError.args)
            raise

    def pdf_export(self, mail=True, name=None):

        # generate pdf
        return self.document.generate_pdf(clean_tex=True)
        #
        # if mail and name is not None or name == "":
        #     mail = Mail()
        #     mail.sendMail(name=name)

    def latex_export(self, mail=False, name=None):

        # generate latex
        self.document.generate_tex()

        if mail and name is not None or name == "":
            mail = Mail()
            mail.sendMail(name=name)
