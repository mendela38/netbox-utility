from baseClass import BaseClass

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


# Author: Rishabh Bansal
# Modified by: Nelson Dick Kelechi

class Mail(BaseClass):

    def __init__(self):
        super().__init__()
        self.email = self.configuration.read_config("Mailing", "email")
        self.password = self.configuration.read_config("Mailing", "password")
        self.stmp = self.configuration.read_config("Mailing", "stmp")
        self.port = self.configuration.read_config("Mailing", "port")

        recipients = self.configuration.read_config("Mailing", "to").split(",")

        if len(recipients) > 1:
            for recipient in self.recipients:
                self.to.append(recipient)
        else:
            self.to = recipients

    def sendMail(self, message=None, attachment=True, name=""):
        # instance of MIMEMultipart
        msg = MIMEMultipart()

        # storing the senders email address
        msg['From'] = self.email

        # storing the receivers email address
        msg['To'] = self.recipients

        # storing the subject
        msg['Subject'] = name

        initialMessage = "Hi,\nThis is automated message from netboxUtils.\nPlease do reply to this email."
        if message is None:
            body = initialMessage
        else:
            body = "%s\n\n%s" % (initialMessage, message)

        # attach the body with the msg instance
        msg.attach(MIMEText(body, 'plain'))

        if attachment:
            # open the file to be sent
            filename = name
            path = "%s%s" % (self.configuration.read_config("Path", "report_path"), name)
            attachment = open(path, "rb")

            # instance of MIMEBase and named as p
            p = MIMEBase('application', 'octet-stream')

            # To change the payload into encoded form
            p.set_payload((attachment).read())

            # encode into base64
            encoders.encode_base64(p)

            p.add_header('Content-Disposition', "attachment; filename= %s" % filename)

            # attach the instance 'p' to instance 'msg'
            msg.attach(p)

        # creates SMTP session
        s = smtplib.SMTP(self.stmp, int(self.port))

        # start TLS for security
        s.starttls()

        # Authentication
        s.login(self.email, self.password)

        # Converts the Multipart msg into a string
        text = msg.as_string()

        # sending the mail
        s.sendmail(self.email, self.to, text)

        # terminating the session
        s.quit()
