#!/usr/bin/env python3
from pylatex import Head, Foot, simple_page_number


from reports.report import Report


class RackReport(Report):

    def __init__(self, document_name):
        super().__init__(document_name)

    def generate_rack_report(self, mail=True):
        dcim = self.netbox.dcim
        tenancies = self.netbox.tenancy

        # Query database for rack information
        racks_info = dcim.racks.get(name=self.item_name).serialize()

        # Set header and footer preferences
        self.set_header(self.item_name, Head("C"))
        self.set_header(simple_page_number(), Foot("R"))

        # Extract rack report information
        rack_info = dict(Site=dcim.sites.get(racks_info['site']),
                         Group=dcim.rack_groups.get(racks_info['group']),
                         FacilityID=racks_info['facility_id'],
                         Tenant=tenancies.tenants.get(racks_info['tenant']),
                         Role=dcim.rack_roles.get(racks_info['role']), SerialNumber=racks_info['serial'],
                         Devices=len(dcim.devices.filter(rack_id=racks_info['id'])))

        # Rack dimension info
        dim_info = dict(Type=racks_info['type'], Width=racks_info['width'], uHeight=racks_info['u_height'])

        # Non Racked devices information
        non_racked_devices = dict(Name="", Role="", Type="", Parent="")

        # Rack Reservation information
        reservations = dcim.rack_reservations.filter(rack=racks_info['id'])
        data = dict(Rack=racks_info, Dimensions=dim_info, Non_Racked_Devices=non_racked_devices,
                    Comments=racks_info['comments'], Reservations=reservations)

        self.add_content("Created: %s - Updated: %s\n" % (racks_info['created'], racks_info['last_updated'])) \
            .create_section("Rack Infomations").add_content(rack_info) \
            .create_section("Dimensions").add_content(dim_info) \
            .create_section("Non-Racked Devices").add_content(non_racked_devices) \
            .create_section("Comments").add_content(racks_info['comments'])

        # Reservations
        self.create_section("Reservations")
        for reservation in reservations:
            current_rack_reservation = reservation.serialize()
            reserve = dict(Units=current_rack_reservation['units'],
                           Tenant=tenancies.tenants.get(current_rack_reservation['tenant']),
                           Description=current_rack_reservation['description'])
            self.add_content(reserve)

        # Get devices information that are on the rack
        self.create_section("Devices")
        devices = dcim.devices.filter(rack_id=racks_info['id'])
        for device in devices:
            device_details = dcim.devices.get(name=device).serialize()
            self.sub_section(title=("%s\n" % device))
            self.add_content(device_details)

