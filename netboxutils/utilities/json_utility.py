#!/usr/bin/env python3

import json


# read_data and find_by_class method; author: Martijn Pieters; Modified by: Nelson; link:
# https://stackoverflow.com/questions/13911655/python-iterate-a-subprocess-that-returns-json?utm_medium=organic
# &utm_source=google_rich_qa&utm_campaign=google_rich_qa


def append(path='data/result2.json', data=None):
    if data is None:
        data = []
    with open(path, 'w+') as outfile:
        json.dump(data, outfile)


def write(path='data/result.json', data=None):
    if data is None:
        data = []
    with open(path, 'w') as outfile:
        json.dump(data, outfile)


class JsonUtility:

    def __init__(self, file_path):
        if file_path:
            self.file_path = file_path
        else:
            raise ValueError("Path to JSON file not given.")

    @property
    def read_file(self):
        """
        loads json file.
        :return: dictionary of the json file
        """
        # return json.loads(self.file_path)
        json_date = open(self.file_path).read()
        return json.loads(json_date)

    def find_by_key(self, data, key_name, key_value):
        """
        recursive search by class
        :param key_name:
        :param filter_value:
        :param filter_key:
        :param data: data filter through
        :param key_value: class name in the json file
        """

        for entry in data.get('children', []):
            if entry is not None:
                # if type(key_name) is not dict:
                if entry.get(key_name) == key_value:
                    yield entry

            for child in self.find_by_key(entry, key_name, key_value):
                if child is not None:
                    yield child

    def find_by_key_filter(self, key="class", value="", filter_key=None, filter_value=None):
        """
        Filters through Json file in memory for specify values using key and value pairs
        :param key:
        :param value:
        :param filter_key:
        :param filter_value:
        :return:
        """
        results = []
        items = self.find_by_key(self.read_file, key, value)
        for item in items:
            if filter_value is not None or filter_key is not None:
                try:
                    if filter_value in item.get(filter_key):
                        results.append(item)
                except TypeError as type_error:
                    print(type_error)
            else:
                results.append(item)

        return results
