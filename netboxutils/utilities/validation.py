#!/usr/bin/env python3

import os
from builtins import ValueError

from baseClass import BaseClass
from connection import ApiUtility


def validate_item(type, item_name):
    # Check if BaseClass._device_name value is not empty
    global item
    if item_name is not None:

        # Loop through item list in netbox
        api_instance = ApiUtility()
        if type == "Device":
            item = api_instance.netbox.dcim.devices.get(name=item_name)
        elif type == "Rack":
            item = api_instance.netbox.dcim.racks.get(name=item_name)
        else:
            raise Exception("No type: %s" % type)

        # Validation state
        if item is None:
            print("%s named '%s' was not found." % (type, item_name))
            return False
        else:
            BaseClass.item_name = item_name
            return True
    raise Exception("%s name is required." % type)


def validate_device_name():
    """
    Validate if device is already stored.
    :return: returns 'True' if found else raise Exception
    """

    # Check if BaseClass._device_name value is not empty
    if BaseClass.device_name is not None:

        # Loop through device list in netbox
        api_instance = ApiUtility()
        device = api_instance.netbox.dcim.devices.get(name=BaseClass.device_name)

        # Validation state
        if device is None:
            print("Device named '{}' was not found.".format(BaseClass.device_name))
            return False
        else:
            BaseClass.item_name = device
            return True
    raise Exception("Device name is required.")


def validate_rack_name(rack_name):
    """
    Validate if device is already stored.
    :param rack_name: Name of the rack to query
    :param BaseClass._device_name: Device BaseClass._device_name entered by the user
    :param api_instance: Netbox API connection instance
    :return: returns 'True' if found else raise Exception
    """

    # Check if BaseClass._device_name value is not empty
    if rack_name is not None:

        # Loop through device list in netbox
        api_instance = ApiUtility()
        rack = api_instance.netbox.dcim.racks.get(name=rack_name)

        # Validation state
        if rack is None:
            raise ValueError("Rack '{}' was not found.".format(rack_name))
        else:
            return True

    raise ValueError("Rack name is Required.")


def validate_existence():
    """
    Validation if BaseClass._json_path given has a file associated with that.
    :param BaseClass._json_path: Path of file given
    :return: Return 'True' if BaseClass._json_path found else raise Exception
    """
    if os.path.exists(BaseClass.json_path) == 1:
        return True
    raise Exception("File Configuration '{}' does not exist.".format(BaseClass.json_path))


def validate_format(path, file_format):
    """
    Validate Format of the file.
    :param BaseClass._json_path: Path to file to be validated
    :param file_format: format excepted
    :return: 'True' if found else raise Exception
    """
    if path.endswith(file_format):
        return True

    raise Exception("Format of your file is invalid. Format allowed is (*.{})".format(file_format))


def validate_json_path():
    """
    Validate JSON File
    :param BaseClass._json_path: Path to JSON file
    :return: 'True' if found and 'False' if not.
    """
    if validate_existence():
        if validate_format(BaseClass.json_path, "json"):
            return True


def validate_config_path():
    """
    Validate configuration BaseClass._json_path.
    :return: 'True' if found and 'False' if not.
    """
    if validate_existence():
        if validate_format(BaseClass.config_path, "ini"):
            return True


def isEmpty(elementToValidateValue, elementTovalidateStr):
    if elementToValidateValue is None:
        raise Exception("'%s' can not be empty" % elementTovalidateStr)


def is_directory(path):
    if not os.path.isdir(path):
        return False
    return True
