#!/usr/bin/env python3

import csv
import os.path


class CsvWriteReader:

    def __init__(self, path):
        self.path = path

    def read_file(self):
        # Check if path is correct
        if self.file_exist(self.path):

            result_csv = []
            with open(self.path, 'r') as csv_file:
                csv_reader = csv.DictReader(csv_file)

                for line in csv_reader:
                    result_csv.append(line)

            return result_csv
        else: # Show Error message
            print("File path: '" + self.path + "' does not exist")

    # Check if file enter exists
    def file_exist(self, path):
        try:
            return os.path.isfile(path)
        except FileNotFoundError:
            return False


# 'names.csv'
reading = CsvWriteReader('names.csv')
results = reading.read_file()
print(results)



