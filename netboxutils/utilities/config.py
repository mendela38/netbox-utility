#!/usr/bin/env python3

import configparser

def set_conf_path(configuration=None):
    if configuration == "current" or configuration is None:
        return "/home/nelson/Documents/netbox-utility/config.ini"
    else:
        return configuration

#############################################################################################
# https://martin-thoma.com/configuration-files-in-python/
# author: Martin Thomas
# Modified: Nelson Dick Kelechi
#########################################################################################


class Configuration:

    def __init__(self, _path):
        if _path:
            self.path = _path
        else:
            self.path = "/home/nelson/Documents/netbox-utility/config.ini"

        self.config = configparser.ConfigParser()

    def read_config(self, section_name, option):
        try:
            self.config.read(self.path)
            return self.config.get(section=section_name, option=option)
        except ValueError:
            print("Did not found value for '" + option + "' under section '" + section_name + "'\n@Path: " + self.path)

    def write_config(self, section, key, value):
        self.config[section] = {key + ' : ' + value}
        with open(self.path, 'w') as configfile:
            self.config.write(configfile)