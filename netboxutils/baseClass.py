#!/usr/bin/env python3
from notifications.notify import Notify
from utilities.config import Configuration

import time
import datetime


def timestamp():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H%M%S')

class BaseClass:
    config_path = ''  # type: str
    json_path = ''  # type: str
    device_name = ''  # type: str
    basePath = ''  # type: str
    msgErr = ''  # type: str
    item_name = ''  # type: str

    def __init__(self):
        """
        Base Class Initialization
        """
        self.configuration = Configuration(_path=BaseClass.config_path)
        self.notify = Notify(_url=self.configuration.read_config(section_name="Notification", option="hook_url"))
        # self.json = JsonUtility(BaseClass.json_path)

